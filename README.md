# Audio Input Streamer
This project uses [darkice](http://darkice.org/) to stream audio from a 
microphone to an [icecast](https://icecast.org/) server.

It provides a docker image and a docker-compose file for execution.

This is tested on a Raspberry PI 2 B with an https://respeaker.io/2_mic_array.

The icecast server is not part of this repository.

## Build darkice-streamer on PI
To build the darkice docker image on a raspberry pi call

    docker build -t darkice-streamer .

## Run with docker-compose
To run the audio streaming with darkice call

    docker-compose up

### Configuration
The configuration of darkice is as defined in the *darkice.cfg* file.

The default configuration assumes you're running the icecast server on the
same machine. Make sure to change `[icecast2-0] server` for custom setup.
It also uses the default audio capture device, which is likely the microphone
of your device. To use another capture source change `[input] device = default`
to f.e. `device = hw:1,0`. Try `arecord -l` to list available input devices.

For further streaming configuration check [darkice.org](http://darkice.org/).
